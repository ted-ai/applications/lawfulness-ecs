from io import BytesIO

from lxml import etree
from lxml.etree import _Element


class Xml:
    def __init__(self, data: bytes | _Element):
        if isinstance(data, bytes):
            self.root: _Element = etree.parse(BytesIO(data)).getroot()
        else:
            self.root = data

    def all(self, path: str) -> list[str | _Element]:
        try:
            return self.root.xpath(path, namespaces={k: v for k, v in self.root.nsmap.items() if k})
        except Exception as e:
            if str(e) == "Undefined namespace prefix":
                return []
            raise e

    def children(self, path: str) -> list['Xml']:
        return [Xml(element) for element in self.all(path)]

    def first(self, path: str) -> str | _Element | None:
        return next(iter(self.all(path)), None)
