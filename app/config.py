from enum import Enum
from typing import Optional

from pydantic import Field
from pydantic_settings import SettingsConfigDict, BaseSettings

SQS_MAX_MESSAGE_BATCH = 10


class Mode(Enum):
    API = "api"
    TASK = "task"

    @classmethod
    def _missing_(cls, value: str) -> Optional['Mode']:
        value = value.lower()
        return next((member for member in cls if member.value == value), None)


class AppConfig(BaseSettings):
    mode: Mode
    log_level: str
    thread_count: int
    ignore_non_english_notices: bool = False
    force_english_translation: bool = False
    flagged_notices_dynamodb_table_name: str


class AwsConfig(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='AWS_')

    region: str = 'eu-west-1'
    endpoint_url: str | None = None


class DbConfig(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='DB_')

    host: str
    port: str
    name: str
    username: str
    password_ssm_parameter: str
    whitelisted_bodies_table: str


class NewNoticeConfig(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='NEW_NOTICES_')

    queue_url: str
    batch_size: int = Field(..., le=SQS_MAX_MESSAGE_BATCH)


class WhitelistedBodyConfig(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='WHITELISTED_BODY_')

    name_weight: float
    town_weight: float
    postal_code_weight: float
    min_score: float


APP_CONFIG = AppConfig()
AWS_CONFIG = AwsConfig()
DB_CONFIG = DbConfig()
NEW_NOTICES_CONFIG = NewNoticeConfig()
WHITELISTED_BODY_CONFIG = WhitelistedBodyConfig()
