import json
from decimal import Decimal

from boto3_type_annotations.dynamodb import ServiceResource as DynamoDBResource
from pydantic import BaseModel

from config import APP_CONFIG
from repositories.new_notices import Notice
from services.analysis import NoticeFieldAnalysis
from services.extraction import NoticeFields
from services.flagging import NoticeFlags
from utils import logging

LOGGER = logging.logger(__name__)


class NoticeResult(BaseModel):
    notice_id: str
    notice_publication_number: str | None
    flags: NoticeFlags
    field_analysis: NoticeFieldAnalysis
    extracted_fields: NoticeFields


def save(dynamodb: DynamoDBResource, notice_result: NoticeResult, notice: Notice):
    if not notice_result.flags.is_flagged():
        LOGGER.debug(f"Notice {notice_result.notice_id} not saved in DynamoDB because it is not flagged.")
        return
    result_dict = notice_result.model_dump(mode="json")
    result_dict["s3_uri"] = f"s3://{notice.bucket}/{notice.key}"
    table = dynamodb.Table(APP_CONFIG.flagged_notices_dynamodb_table_name)
    table.put_item(Item=result_dict)
    LOGGER.debug(f"Flagged notice {notice_result.notice_id} saved in DynamoDB.")


def _convert_to_dynamodb_dict(dict_: dict) -> dict:
    return json.loads(json.dumps(dict_), parse_float=Decimal)
