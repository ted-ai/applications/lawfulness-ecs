import json
from pathlib import Path
from typing import Iterator

from pydantic import BaseModel, Field

from utils import logging

LOGGER = logging.logger(__name__)

FIELD_PATH = Path(__file__).parent.parent.parent / "resources" / "eforms_fields.json"


class EFormsField(BaseModel):
    id: str
    xpath_absolute: str = Field(..., alias='xpathAbsolute')
    xpath_relative: str = Field(..., alias='xpathRelative')


def absolute_xpath(id_: str) -> str:
    return _FIELDS[id_].xpath_absolute


def relative_xpath(id_: str) -> str:
    return _FIELDS[id_].xpath_relative


def _parse_fields() -> Iterator[EFormsField]:
    fields = json.loads(FIELD_PATH.read_text())
    for field in fields["fields"]:
        yield EFormsField.model_validate(field)
    for field in fields["xmlStructure"]:
        yield EFormsField.model_validate(field)
    LOGGER.info("eForms fields loaded.")


_FIELDS = {field.id: field for field in _parse_fields()}
